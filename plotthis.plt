#! /usr/bin/env gnuplot

set xdata time
set timefmt "%d/%m/%y"
#set xrange ["03/21/95":"03/22/95"]
set yrange [300:2100]
set format x "%H:%M"
set timefmt "%d/%m/%Y %H:%M:%S"
set terminal png size 1024,768
set output "output.png"

set arrow from graph 0,first 800 to graph 1, first 800 nohead lw 2 lc rgb "#00FF00" front
set arrow from graph 0,first 1000 to graph 1, first 1000 nohead lw 2 lc rgb "#009F00" front
set arrow from graph 0,first 1500 to graph 1, first 1500 nohead lw 2 lc rgb "#FFFF00" front
set arrow from graph 0,first 2000 to graph 1, first 2000 nohead lw 2 lc rgb "#FF0000" front
set label "1" at graph 0.8, first -2

plot "frame.log" using 1:3 with linespoints t "CO2 (ppm)"
