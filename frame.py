#! /usr/bin/env python3

from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import QTimer, Qt
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
import sys  # We need sys so that we can pass argv to QApplication

import mh_z19
import os
from random import randint

from datetime import datetime

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setWindowTitle("CO2 Messung")

        self.setStyleSheet("QMainWindow {background: 'black';}");

        self.layout = QtWidgets.QVBoxLayout()
        self.widget = QtWidgets.QWidget()
        self.widget.setLayout(self.layout)

        self.label = QtWidgets.QLabel()
        self.label.setAlignment(Qt.AlignCenter)
        
        font = QtGui.QFont("Arial", 80, QtGui.QFont.Bold);
        self.label.setFont(font)
        self.label.setStyleSheet("QLabel { background-color : black; color : white; }")

        self.graphWidget = pg.PlotWidget()

        self.layout.addWidget(self.label)
        self.layout.addWidget(self.graphWidget)

        self.setCentralWidget(self.widget)

        self.x = list(range(28800))
        self.y = [randint(0,0) for _ in range(28800)]

        #self.graphWidget.setBackground('b')
        mh_z19.abc_off(serial_console_untouched=True)

        pen = pg.mkPen(color=(255, 0, 0))
        self.data_line = self.graphWidget.plot(self.x, self.y, pen=pen)

        self.timer = QtCore.QTimer()
        self.timer.setInterval(2000)
        self.timer.timeout.connect(self.update_plot_data)
        self.timer.start()

        self.ltimer = QtCore.QTimer()
        self.ltimer.setInterval(1000 * 60)
        self.ltimer.timeout.connect(self.update_log)
        self.ltimer.start()

    def update_plot_data(self):
        data = int(mh_z19.read(serial_console_untouched=True)['co2'])
        self.x = self.x[1:]  # Remove the first y element.
        self.x.append(self.x[-1] + 1)  # Add a new value 1 higher than the last.

        self.y = self.y[1:]  # Remove the first
        self.y.append(data)

        self.data_line.setData(self.x, self.y)  # Update the data.
        self.label.setText(str(self.y[-1]) + " ppm")

    def update_log(self):
        data = int(mh_z19.read(serial_console_untouched=True)['co2'])
        with open("/home/pi/co2frame/frame.log", "a") as myfile:
            now = datetime.now()
            dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            data = int(mh_z19.read(serial_console_untouched=True)['co2'])
            line = """{} {}\n""".format(dt_string, data)
            myfile.write(line)

app = QtWidgets.QApplication(sys.argv)
w = MainWindow()
w.setWindowState(Qt.WindowMaximized)
w.show()
sys.exit(app.exec_())


